﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    private Rigidbody myBody;
    [SerializeField] private float maxSpeed;
    public GameObject playerTrail;
    private bool speedLimitReleased;
    //internal int levelIndex;

    private void OnEnable()
    {
        myBody = GetComponent<Rigidbody>();
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        playerTrail.GetComponent<TrailRenderer>().Clear();
        playerTrail.transform.parent = transform;
        speedLimitReleased = false;
        //levelIndex = GameObject.FindWithTag("GameController").GetComponent<GameController>().levelIndex;
    }

    private void OnDisable()
    {
        myBody.velocity = Vector3.zero;
        myBody.isKinematic = true;
    }

    internal void ReleaseSpeedLimit()
    {
        StartCoroutine(RelSpeedLimit(1f));
    }

    private IEnumerator RelSpeedLimit(float time)
    {
        speedLimitReleased = true;
        yield return new WaitForSeconds(time);
        speedLimitReleased = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Water"))
        {
            myBody.velocity = myBody.velocity / 5;
            StartCoroutine(WaitAndDisable(0.0f));
        } 
    }

    private IEnumerator WaitAndDisable(float time)
    {
        yield return new WaitForSeconds(time);
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        StartCoroutine(PlayerOut(3f));
    }

    private IEnumerator PlayerOut(float time)
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().RemoveCurrentPlayer();
        Invoke("NextPlayerSpawn", 1f);
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
        playerTrail.transform.parent = null;
    }

    private void NextPlayerSpawn()
    {
        // TODO Player gets spawned unwantedly after the level has changed 
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().PlayerWipedOut();
    }

    void Update()
    {
        Vector3 myVelo = myBody.velocity;
        if (maxSpeed < myVelo.magnitude && !speedLimitReleased)
        {
            myBody.velocity = myVelo.normalized * maxSpeed;
        }

        // NOTE: for debugging
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(PlayerOut(0f));
        }

        if(Input.GetKeyDown(KeyCode.RightShift))
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().GoalReached();
        }
    }

}

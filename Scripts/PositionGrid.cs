﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionGrid : MonoBehaviour {

    private List<GameObject> gridNodes;
    [SerializeField] Transform topLeftCorner;
    [SerializeField] Transform bottomRightCorner;

    [SerializeField] int amountNodesHorizontally;
    [SerializeField] int amountNodesVertically;

    [SerializeField] Transform nodesParent;
    [SerializeField] Transform singleNode;

    void Start () {
        if (nodesParent.childCount <= 0) GenerateGrid();
        //InitializeGridNodes();
	}

    // GameController handles initialization call
    internal void InitializeGridNodes()
    {
        gridNodes = new List<GameObject>();
        for (int i = 0; i < nodesParent.childCount; i++)
        {
            gridNodes.Add(nodesParent.GetChild(i).gameObject);
        }
    }

    private void DestroyOldGrid()
    {
        if (nodesParent.childCount > 0)
        {
            for (int i = 0; i < nodesParent.childCount; i++)
            {
                DestroyImmediate(nodesParent.GetChild(i).gameObject);
            }
        }
    }

    private void GenerateGrid()
    {
        //DestroyOldGrid();

        float xDist = Mathf.Abs(topLeftCorner.position.x - bottomRightCorner.position.x);
        float yDist = Mathf.Abs(topLeftCorner.position.y - bottomRightCorner.position.y);

        float xInterval = xDist / (amountNodesHorizontally-1);
        float yInterval = yDist / (amountNodesVertically-1);
        
        for (int i = 0; i < amountNodesVertically; i++)
        {
            for (int j = 0; j < amountNodesHorizontally; j++)
            {
                Transform node = Instantiate(singleNode);
                node.position = new Vector3(topLeftCorner.position.x + j * xInterval, topLeftCorner.position.y - i * yInterval, 0);
                node.parent = nodesParent;
            }
        }
    }

# if UNITY_EDITOR
    public void GenerateGridEditor()
    {
        //DestroyOldGrid();

        float xDist = Mathf.Abs(topLeftCorner.position.x - bottomRightCorner.position.x);
        float yDist = Mathf.Abs(topLeftCorner.position.y - bottomRightCorner.position.y);

        float xInterval = xDist / (amountNodesHorizontally - 1);
        float yInterval = yDist / (amountNodesVertically - 1);

        for (int i = 0; i < amountNodesVertically; i++)
        {
            for (int j = 0; j < amountNodesHorizontally; j++)
            {
                Transform node = Instantiate(singleNode);
                node.position = new Vector3(topLeftCorner.position.x + j * xInterval, topLeftCorner.position.y - i * yInterval, 0);
                node.parent = nodesParent;
            }
        }
    }
# endif

    internal Vector3 GiveRandomPosition()
    {
        if (gridNodes.Count > 0)
        {
            int randomVectoreIndex = Random.Range(0, gridNodes.Count);
            GameObject node = gridNodes[randomVectoreIndex];
            gridNodes.Remove(node);
            return node.transform.position;
        }
        return Vector3.zero;
    }



}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {

    [SerializeField] private GameObject touchGame;
    [SerializeField] private GameObject touchMusic;

    void Start () {
        touchGame.SetActive(true);
        touchMusic.SetActive(false);
    }

    internal void ChangeView()
    {
        if (touchGame.activeInHierarchy)
        {
            Camera.main.GetComponent<CameraPosition>().ChangeToMusicView();
            touchGame.SetActive(false);
            touchMusic.SetActive(true);
            MusicViewRelatedActions();
        }
        else if (touchMusic.activeInHierarchy)
        {
            Camera.main.GetComponent<CameraPosition>().ChangeToGameView();
            touchMusic.SetActive(false);
            touchGame.SetActive(true);
            GameViewRelatedActions();
        }
        StartCoroutine(SetShakeActiveAgain());
    }

    private void GameViewRelatedActions()
    {
        //GameObject ammoSwirl = GameObject.FindWithTag("Ammo");
        GameObject.FindWithTag("Ammo").GetComponent<AmmoAnimation>().MoveToGamePosition();
    }

    private void MusicViewRelatedActions()
    {
        GameObject.FindWithTag("GameController").GetComponent<GameController>().DisablePromptForShake();
        GameObject.FindWithTag("GameController").GetComponent<PlayerProgression>().tutorialPlayed++;
    }

    private IEnumerator SetShakeActiveAgain()
    {
        yield return new WaitForSeconds(2f);
        GetComponent<ShakeAccelometer>().enabled = true;
    }
}

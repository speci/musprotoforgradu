﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour {

    public GameObject player;
    public GameObject water;
    public GameObject level;

    private GameObject currentPlayer;
    private GameObject currentWater;

    private Vector3 playerStartPosition;

    [SerializeField] private float waitTime;
    [SerializeField] private float animationTime;
    [SerializeField] private float playerSpawnTime;
    [SerializeField] private float playerWait;

    [SerializeField] private float goalXPosition;

    public List<GameObject> toBeAnimated;

    public int movablesMinAmount;
    public int movablesMaxAmount;
    public List<GameObject> allMovables;
    private List<GameObject> levelMovables;
    private List<GameObject> levelObstacles;

    private Colors customColors;

    [SerializeField] PositionGrid grid;

    [SerializeField] PlayerProgression playerProgression;

    [SerializeField] private GameObject promptForShake;

    void Awake()
    {
        levelMovables = new List<GameObject>();
        levelObstacles = new List<GameObject>();
        customColors = GameObject.FindWithTag("Colors").GetComponent<Colors>();
        SetUIColors();
        playerProgression = GetComponent<PlayerProgression>();
    }

    internal void CreateLevel()
    {
        grid.InitializeGridNodes();

        if (currentWater == null) CreateWater();
        CreatePlayer(true, false);

        LotMovables(GetComponent<ProgressionTranslator>().GiveMovablesAmount(movablesMinAmount, movablesMaxAmount));
        toBeAnimated.Add(currentPlayer);
        SetColors();
        AnimateThings(1f, false);

        AddObstacles();
    }

    private void CreateWater()
    {
        currentWater = ObjectPoolManager.GetNext(water);
        currentWater.transform.position = new Vector3(0, -11f, -3.5f);
        currentWater.transform.parent = level.transform;
        currentWater.AddComponent<ToBeMovedTo>().moveTo = new Vector3(0, -7.0f, -3.5f);
        toBeAnimated.Add(currentWater);
    }

    internal void RemoveCurrentPlayer()
    {
        currentPlayer = null;
    }

    private void AddObstacles()
    {
        int obstacleAmount = GetComponent<ProgressionTranslator>().GiveObstacleAmount();
        int currentTier = GetComponent<ProgressionTranslator>().GiveCurrentTier();
        GameObject tierObst = GetComponent<ObstacleManager>().GiveTierObstacle(currentTier);
        if (obstacleAmount > 0)
        {
            for (int i = 0; i < obstacleAmount; i++)
            {
                GameObject obst = ObjectPoolManager.GetNext(tierObst);
                obst.transform.position = grid.GiveRandomPosition();
                obst.transform.parent = level.transform;
                levelObstacles.Add(obst);

                iTween.ColorTo(obst, iTween.Hash("color", customColors.obstacle, "time", 0.0f));
                iTween.ScaleFrom(obst, iTween.Hash("scale", Vector3.zero, "time", 0.5f));
            }
        }

        if (obstacleAmount >= 3 && playerProgression.tutorialPlayed < 1)
        {
            Invoke("ActivatePromptForShake", 4f);
        }
    }

    private void ActivatePromptForShake()
    {
        promptForShake.SetActive(true);
    }

    internal void DisablePromptForShake()
    {
        promptForShake.SetActive(false);
        if (playerProgression.tutorialPlayed < 1) Invoke("PromptForTouch", 1f);
    }

    // Make music component to prompt for touch
    private void PromptForTouch()
    {
        GameObject.FindWithTag("TracksUI").GetComponent<TracksTenoriUI>().PromptForTouch();
    }

    private void SetUIColors()
    {
        Camera.main.backgroundColor = customColors.background;
        GameObject playButton = GameObject.FindWithTag("Button");
        iTween.ColorTo(playButton, iTween.Hash("color", customColors.logo, "time", 0.0f));
        iTween.ColorTo(playButton.transform.GetChild(0).gameObject, iTween.Hash("color", customColors.background, "time", 0.0f));

        TextMeshPro[] logoTexts = FindObjectsOfType<TextMeshPro>();
        logoTexts[0].color = customColors.logo;
        logoTexts[1].color = customColors.logo;
    }

    private void SetColors()
    {
        iTween.ColorTo(currentWater, iTween.Hash("color", customColors.water, "time", 0.0f));
    }

    internal void ResetMovableColors()
    {
        for (int i = 0; i < levelMovables.Count; i++)
        {
            MovableLogic mova = levelMovables[i].transform.GetChild(0).GetComponent<MovableLogic>();
            if (!mova.beingDragged) mova.ResetColor();
        }
    }

    private void LotMovables(int movablesAmount)
    {
        for (int i = 0; i < movablesAmount; i++)
        {
            GameObject movable = ObjectPoolManager.GetNext(allMovables[UnityEngine.Random.Range(0, allMovables.Count)]);

            // Move animation
            Vector2 mypos = grid.GiveRandomPosition();
            movable.AddComponent<ToBeMovedTo>().moveTo = mypos;
            toBeAnimated.Add(movable);
            if (mypos.x > 0) movable.transform.position = new Vector2(-4.3f, mypos.y);
            else movable.transform.position = new Vector2(4.3f, mypos.y);

            // Random scale
            float scaleRandom = UnityEngine.Random.Range(0.8f, 1.2f);
            Transform logicAndCollider = movable.transform.GetChild(0);
            Vector3 modifyScale = movable.transform.localScale * scaleRandom;
            logicAndCollider.GetComponent<MovableLogic>().ScaleMe(modifyScale);

            // Rotate a little
            int one = Probability.IsProbability(0.5f) ? 1 : -1;
            logicAndCollider.GetComponent<MovableLogic>().RotateMe(new Vector3(0, 0, one * UnityEngine.Random.Range(45, 55)));

            movable.transform.parent = level.transform;

            // Number text
            logicAndCollider.GetComponent<MovableLogic>().orderNumber = i;
            Transform textThing = movable.transform.GetChild(1);
            textThing.GetComponent<TextMesh>().text = "" + (i+1);
            textThing.GetComponent<TextMesh>().color = customColors.movableNumber;

            iTween.ColorTo(logicAndCollider.gameObject, iTween.Hash("color", customColors.movableDefault, "time", 0.0f));

            // Give a specific instrument to the movable
            List<int> melodicInstruments = GameObject.FindWithTag("MusicManager").GetComponent<PlaybackEngine>().currentSong.GiveInstrumentsByType(Instrument.InstrumentType.Melodic);
            logicAndCollider.GetComponent<MovableLogic>().instrumentIndex = melodicInstruments[i % melodicInstruments.Count];

            levelMovables.Add(movable);
        }
    }

    private void AnimateThings(float physicsReleaseTime, bool afterWipeout)
    {
        int waitIndex = 0;

        // Animate movables
        for (int i = 0; i < toBeAnimated.Count-1; i++)
        {
            float myWait = waitTime * waitIndex;
            ToBeMovedTo toBeMovedTo = toBeAnimated[i].GetComponent<ToBeMovedTo>();
            Vector3 moveTo = toBeMovedTo.moveTo;
            iTween.MoveTo(toBeAnimated[i], iTween.Hash("position", moveTo, "time", animationTime, "delay", myWait));
            Destroy(toBeMovedTo);
            waitIndex++;
        }

        // Animate player sligthly differently
        float playerAnimateWait = waitTime * waitIndex + playerWait;
        ToBeMovedTo playerMovedTo = toBeAnimated[toBeAnimated.Count - 1].GetComponent<ToBeMovedTo>();
        Vector3 playerMoveTo = playerMovedTo.moveTo;
        GameObject thePlayer = toBeAnimated[toBeAnimated.Count - 1];

        iTween.Stop(thePlayer);
        iTween.MoveTo(thePlayer, iTween.Hash("position", playerMoveTo, "time", animationTime, "delay", playerAnimateWait, "name", "playerMove"));
        Destroy(playerMovedTo);

        toBeAnimated.Clear();
        float playerReleaseTime;
        if (afterWipeout) playerReleaseTime = physicsReleaseTime;
        else playerReleaseTime = playerAnimateWait + animationTime + physicsReleaseTime;
        //print(playerReleaseTime);
        StartCoroutine(ReleasePlayerPhysics(playerReleaseTime));
    }

    private void CreatePlayer(bool lotPosition, bool afterWipeout)
    {
        currentPlayer = ObjectPoolManager.GetNext(player);
        float playerX;
        if (lotPosition)
        {
            playerX = UnityEngine.Random.Range(-goalXPosition, goalXPosition);
            playerStartPosition = new Vector3(playerX, 7.5f, -1.8f);
        }
        else playerX = playerStartPosition.x;
        currentPlayer.transform.position = playerStartPosition;
        currentPlayer.transform.parent = level.transform;
        currentPlayer.AddComponent<ToBeMovedTo>().moveTo = new Vector2(playerX, 5.0f);
        iTween.ColorTo(currentPlayer.transform.GetChild(0).gameObject, iTween.Hash("color", customColors.movableDefault, "time", 0.0f));

        toBeAnimated.Add(currentPlayer);
        if (afterWipeout) AnimateThings(1.5f, true);
        //else AnimateThings(1f, false);
    }

    internal void PlayerWipedOut()
    {
        //if (levelIndexWhenWipedOut == levelIndex)
        //{
        if (currentPlayer == null)
        {
            ResetMovableColors();
            CreatePlayer(false, true);
            ActivateAgainObstacles();
            //}
        }
    }
    private void ActivateAgainObstacles()
    {
        for (int i = 0; i < levelObstacles.Count; i++)
        {
            Obstacle obst = levelObstacles[i].GetComponent<Obstacle>();
            if (!obst.IsObstacleActivated() && !obst.IsObstacleDestroyed())
            {
                obst.SendMessage("ActivateAgain");
                iTween.ScaleFrom(obst.gameObject, iTween.Hash("scale", Vector3.zero, "time", 0.5f));
            }
        }
    }

    private IEnumerator ReleasePlayerPhysics(float time)
    {
        yield return new WaitForSeconds(time);
        currentPlayer.GetComponent<Rigidbody>().isKinematic = false;
        currentPlayer.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    internal void CheckIfGoalReached(int playerCurrentMovable)
    {
        if ((playerCurrentMovable + 1) == levelMovables.Count)
        {
            GoalReached();
        }
    }
    
    internal void GoalReached()
    {
        StopAllCoroutines();
        StartCoroutine(WaitAndCreate(1f));
    }

    private void DisableMovables()
    {
        for (int i = 0; i < levelMovables.Count; i++)
        {
            levelMovables[i].transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
            levelMovables[i].transform.GetChild(0).GetComponent<MovableLogic>().HardResetColor();
            levelMovables[i].SetActive(false);
        }
        levelMovables.Clear();
    }

    private void DisableObstacles()
    {
        if (levelObstacles.Count > 0)
        {
            for (int i = 0; i < levelObstacles.Count; i++)
            {
                if (levelObstacles[i].activeInHierarchy)
                {
                    levelObstacles[i].SendMessage("DisableObstacle");
                }
            }
            levelObstacles.Clear();
        }
    }

    private IEnumerator WaitAndCreate(float time)
    {
        DisableObstacles();
        yield return new WaitForSeconds(time);
        if (currentPlayer != null) currentPlayer.SetActive(false);
        DisableMovables();
        playerProgression.IncreaseLevelsPlayer();
        playerProgression.IncreaseCurrentLevel();
        playerProgression.SaveProgress();
        CreateLevel();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

    bool gameStarted;

    public GameController gameController;
    [SerializeField] private List<GameObject> toMoveAway;
    [SerializeField] private float moveLeft;
    [SerializeField] private float animationTime;
    [SerializeField] private float waitTime;

    internal void StartTheGame()
    {
        if (gameStarted) return;
        int waitIndex = 0;
        foreach (var item in toMoveAway)
        {
            Vector2 previousPos = item.transform.position;
            float myWait = waitTime * waitIndex;
            iTween.MoveTo(item, iTween.Hash("position", new Vector3(previousPos.x - moveLeft, previousPos.y, 0), "time", animationTime - myWait, "delay", myWait /*", oncomplete", "AnimationsComplete"*/) );
            waitIndex++;
        }
        StartCoroutine(AnimsComplete(animationTime - 1f));
        gameStarted = true;
        GameObject.FindWithTag("MusicManager").GetComponent<PlaybackEngine>().SetEngineActive(true);
    }

    private IEnumerator AnimsComplete(float wait)
    {
        yield return new WaitForSeconds(wait);
        StartCoroutine(ActivateShakeInput());
        gameController.CreateLevel();
    }

    private IEnumerator ActivateShakeInput()
    {
        yield return new WaitForSeconds(0.5f);
        FindObjectOfType<ShakeAccelometer>().enabled = true;
    }

}

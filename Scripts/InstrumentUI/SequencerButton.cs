﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequencerButton : MonoBehaviour {

    public string buttonNumber;
    public TracksTenoriUI tracksUI;
    public bool buttonOn = false;

    internal List<GameObject> outputs;
    internal bool prompting;

    internal void SetPrompting(bool value)
    {
        prompting = value;
    }

    private void Start()
    {
        tracksUI = GameObject.FindWithTag("TracksUI").GetComponent<TracksTenoriUI>();
        outputs = new List<GameObject>();
    }

    internal void MapOutputs()
    {
        if (outputs.Count > 0)
        {
            for (int i = 0; i < outputs.Count; i++)
            {
                outputs[i].GetComponent<MovableLogic>().MoveOutput();
                //Debug.Log("map");
            }
        }
    }

    //internal void Touched()
    //{
    //    tracksUI.TriggerSequencer(buttonNumber);
    //}

    internal void AddOutput(GameObject output)
    {
        if (output != null && !outputs.Contains(output))
        {
            outputs.Add(output);
        }
    }
}

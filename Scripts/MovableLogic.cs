﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableLogic : MonoBehaviour {

    [SerializeField] internal int orderNumber;
    private Color movableActivated;
    private Color movableDefault;
    internal bool beingDragged;

    internal Vector3 currentScale;
    internal int instrumentIndex;

    private void OnEnable()
    {
        GameObject model = GetModel();
        model.GetComponent<MeshRenderer>().enabled = true;
        HardResetColor();
    }

    private void OnDisable()
    {
        ResetRotation();
        ResetScale();
    }

    private void Awake()
    {
        Colors colors = GameObject.FindWithTag("Colors").GetComponent<Colors>();
        movableActivated = colors.movableActivated;
        movableDefault = colors.movableDefault;
    }

    internal void HardResetColor()
    {
        EnableCollider(true);
        ChangeColor(movableDefault, 0.0f);
    }

    internal void ResetColor()
    {
        EnableCollider(true);
        ChangeColor(movableDefault, 0.3f);
    }

    private void EnableCollider(bool value)
    {
        Collider box = GetComponent<Collider>();
        box.enabled = value;
    }

    internal void EnableTouched(bool value)
    {
        float waitTime = 1;
        if (value) StartCoroutine(WaitAndEnableTouched(value, 0));
        else StartCoroutine(WaitAndEnableTouched(value, waitTime));
    }

    private IEnumerator WaitAndEnableTouched(bool value, float time)
    {
        yield return new WaitForSeconds(time);
        if (value)
        {
            EnableCollider(false);
            ChangeColor(movableActivated, 0.2f);
            beingDragged = true;
        }
        else
        {
            EnableCollider(true);
            ChangeColor(movableDefault, 0.2f);
            beingDragged = false;
        }
    }

    private void ChangeColor(Color color, float time)
    {
        GameObject model = GetModel(); 
        iTween.ColorTo(model, iTween.Hash("color", color, "time", 0.2f));
    }

    internal void MovableHitAndActivated()
    {
        Collider box = GetComponent<Collider>();
        EnableCollider(false);
        ChangeColor(movableActivated, 0.2f);
        GameObject model = GetModel();
    }

    internal void MoveOutput()
    {
        GameObject model = GetModel();
        iTween.StopByName(model, "output");
        model.transform.localScale = currentScale;
        iTween.ScaleTo(model, iTween.Hash(name, "output", "scale", transform.localScale * 0.8f, "time", 0.1f, "oncomplete", "ReturnScale", "oncompletetarget", gameObject));
    }

    private void ReturnScale()
    {
        GameObject model = GetModel();
        model.transform.localScale = currentScale;
    }

    private GameObject GetModel()
    {
        return transform.parent.GetChild(2).gameObject;
    }

    internal void ResetRotation()
    {
        transform.rotation = Quaternion.identity;
        GameObject model = GetModel();
        model.transform.rotation = Quaternion.identity;
    }

    internal void RotateMe(Vector3 vec)
    {
        transform.Rotate(vec);
        GameObject model = GetModel();
        model.transform.Rotate(vec);
    }

    internal void ScaleMe(Vector3 scale)
    {
        transform.localScale = scale;
        GameObject model = GetModel();
        model.transform.localScale = scale;
        currentScale = scale;
    }

    internal void ResetScale()
    {
        ScaleMe(Vector3.one);
    }
}

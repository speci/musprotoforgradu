﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgression : MonoBehaviour {

    public int levelsPlayed;
    public float currentLevel;

    public int tutorialPlayed;

    private void Awake()
    {
        LoadProgress();
    }

    private void OnApplicationPause(bool pause)
    {
        SaveProgress();
    }

    internal void IncreaseLevelsPlayer()
    {
        levelsPlayed++;
    }

    internal void IncreaseCurrentLevel()
    {
        currentLevel += 1f;
    }

    private void LoadProgress()
    {
        if (ES2.Exists(Constants.playerProgress))
        {
            ES2.Load(Constants.playerProgress, this);
        }
    }

    internal void SaveProgress()
    {
        ES2.Save(this, Constants.playerProgress);
    }

}

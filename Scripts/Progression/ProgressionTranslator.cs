﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionTranslator : MonoBehaviour {

    private LevelProgression progression;
    private PlayerProgression playerProgression;

    //private int obstacleAmountLimit = 6;

    private void Start()
    {
        progression = GetComponent<LevelProgression>();
        playerProgression = GetComponent<PlayerProgression>();
    }

    internal int GiveObstacleAmount()
    {
        float currentlevel = playerProgression.currentLevel;
        float obstProg = progression.GiveProgression(currentlevel);
        int obstAmount = Mathf.FloorToInt(obstProg);
        print("currentLevel " + currentlevel + " ObstProgression " + obstProg + " ObstacleAmount " + obstAmount);

        //Limiting obstacle amount
        //if (obstAmount > obstacleAmountLimit) obstAmount = obstacleAmountLimit;
        return obstAmount;
    }

    internal int GiveCurrentTier()
    {
        float currentlevel = playerProgression.currentLevel;
        int tier = Mathf.FloorToInt(progression.GiveProgression(currentlevel));
        return tier;
    }

    internal int GiveMovablesAmount(int min, int max)
    {
        int movablesAmount = min;

        float currentlevel = playerProgression.currentLevel;
        float curprogression = progression.GiveProgression(currentlevel);
        if (curprogression > -0.3)
        {
            movablesAmount += 2;
            
            if (curprogression > 1.6f)
            {
                movablesAmount = 2 + FractionalPart(curprogression);
            }
        }
        if (movablesAmount >= max) return max;
        return movablesAmount;
    }

    private Int32 FractionalPart(float n)
    {
        string s = n.ToString("#.#########", System.Globalization.CultureInfo.InvariantCulture);
        return Int32.Parse(s.Substring(s.IndexOf(".") + 1));
    }
}

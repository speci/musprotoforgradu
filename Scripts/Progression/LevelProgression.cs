﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgression : MonoBehaviour {

    [SerializeField] private AnimationCurve challengeProgression;
    [SerializeField] private AnimationCurve challengeWave;

    [SerializeField] private float challengeProgressionMultiplier;
    [SerializeField] private float challengeWaveMultiplier;

    [SerializeField] private bool wawe;

    internal float GiveProgression(float value)
    {
        float prog = 0;

        if (value > 3) prog = challengeProgression.Evaluate(value) * challengeProgressionMultiplier + challengeWave.Evaluate(value) * challengeWaveMultiplier;
        else prog = challengeProgression.Evaluate(value) * challengeProgressionMultiplier;
        return prog;
    }

    internal float GiveChallengeProgression(float value)
    {
        float prog = 0;
        prog = challengeProgression.Evaluate(value);
        return prog;
    }

    internal float GiveChallengeWave(float value)
    {
        float prog = 0;
        prog = challengeWave.Evaluate(value);
        return prog;
    }

}

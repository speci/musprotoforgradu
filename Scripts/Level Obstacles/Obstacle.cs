﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    private bool obstacleActivated;
    private bool destroyed;

    public void SetObstacleDestroyed(bool value)
    {
        destroyed = value;
    }

    public bool IsObstacleDestroyed()
    {
        return destroyed;
    }

    public void SetObstacleActivated(bool value)
    {
        obstacleActivated = value;
    }

    public bool IsObstacleActivated()
    {
        return obstacleActivated;
    }

}

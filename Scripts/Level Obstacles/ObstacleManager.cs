﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour {

    [SerializeField] List<GameObject> tierObstacles;

    internal GameObject GiveTierObstacle(int tier)
    {
        return tierObstacles[Mathf.Clamp(tier, 0, tierObstacles.Count - 1)];
    }

}

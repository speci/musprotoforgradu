﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] internal int healthPoints;
    [SerializeField] internal int maxHealth;
    internal int estimatedHealth;

    private void OnEnable()
    {
        healthPoints = maxHealth;
        estimatedHealth = maxHealth;
    }

    internal void DecreaseEstimation()
    {
        estimatedHealth--;
    }

    internal void DecreaseHealth()
    {
        healthPoints--;
        if (healthPoints <= 0) GetComponent<MineScript>().DisableObstacle();
    }

}

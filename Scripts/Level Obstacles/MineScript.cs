﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineScript : Obstacle {

    [SerializeField] float explosionForce;
    [SerializeField] Vector3 defaultScale;

    internal void SetDestroyed()
    {
        SetObstacleDestroyed(true);
    }

    private void OnEnable()
    {
        ResetMine();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Collider coll = collision.collider;
        if (coll.tag.Equals("Player"))
        {
            coll.GetComponent<PlayerScript>().ReleaseSpeedLimit();
            coll.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, 2);
            DisableForNow();
        }
    }

    public void DisableObstacle()
    {
        if (!IsObstacleDestroyed())
        {
            iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", 0.2f));
            SetObstacleActivated(false);
            SetDestroyed();
            StartCoroutine(WaitAndDisable(0.2f));
        }
    }

    private void DisableForNow()
    {
        if (!IsObstacleDestroyed())
        {
            iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", 0.2f));
            Collider coll = GetComponent<Collider>();
            coll.enabled = false;
            SetObstacleActivated(false);
        }
    }

    private void ResetMine()
    {
        GetComponent<Collider>().enabled = true;
        transform.localScale = defaultScale;
        SetObstacleDestroyed(false);
        SetObstacleActivated(true);
    }

    public void ActivateAgain()
    {
        if (!IsObstacleDestroyed()) ResetMine();
    }

    private IEnumerator WaitAndDisable(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
    
}

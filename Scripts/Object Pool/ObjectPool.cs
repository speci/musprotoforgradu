﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public GameObject toBeCreated;
    public int amount;

    private List<GameObject> myPool;

    void Awake()
    {
        if (myPool != null) return;
        WakeUp();
    }

    internal void WakeUp()
    {
        myPool = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            GameObject tobe = Instantiate(toBeCreated);
            tobe.SetActive(false);
            tobe.transform.parent = transform;
            myPool.Add(tobe);
        }
    }

    internal GameObject GetNext(GameObject obj, bool setActive)
    {
        for (int i = 0; i < myPool.Count; i++)
        {
            if (!myPool[i].activeInHierarchy)
            {
                myPool[i].SetActive(setActive);
                return myPool[i];
            }
        }
        return null;
    }

}

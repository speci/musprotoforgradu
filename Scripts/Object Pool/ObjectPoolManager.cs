﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour {

    [SerializeField] List<ObjectPool> pools;

    private void Awake()
    {
        pools = new List<ObjectPool>();
        InitializePools();
    }

    private void InitializePools()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            ObjectPool objPool = transform.GetChild(i).GetComponent<ObjectPool>();
            if (objPool != null)
            {
                pools.Add(objPool);
            }
        }
    }

    public static GameObject GetNext(GameObject prefab, bool setActive)
    {
        GameObject manag = GameObject.FindWithTag("ObjectPoolManager");
        ObjectPoolManager myManager = manag.GetComponent<ObjectPoolManager>();
        return myManager.GetNextInternal(prefab, setActive);
    }

    public static GameObject GetNext(GameObject prefab)
    {
        return GetNext(prefab, true);
    }

    private GameObject GetNextInternal(GameObject prefab, bool setActive)
    {
        GameObject obj = null;
        for (int i = 0; i < pools.Count; i++)
        {
            ObjectPool currentPool = pools[i];
            if (currentPool.toBeCreated.GetHashCode() == prefab.GetHashCode())
            {
                GameObject next = currentPool.GetNext(prefab, setActive);
                if (next != null)
                {
                    if (next.GetComponent<ObjectDisabled>() == null)
                        next.AddComponent<ObjectDisabled>();
                    return next;
                }
                else print("nullia tulee " + prefab);
            }
        }
        print("prefab: " + prefab + " not found");
        return obj;
    }

    public Transform FindMyParent(GameObject me)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].toBeCreated.GetHashCode() == me.GetHashCode())
            {
                return pools[i].transform;
            }
        }
        return null;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlayerHit : MonoBehaviour {

    public int currentMovable = -1;

    private void OnEnable()
    {
        ResetCurrentMovable();
    }

    internal void ResetCurrentMovable()
    {
        currentMovable = -1;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Collider curCollider = collision.collider;
        if (curCollider.tag.Equals("MovableCollider"))
        {
            int movableOrderNumber = curCollider.GetComponent<MovableLogic>().orderNumber;
            if ( (movableOrderNumber - 1) == currentMovable )
            {
                currentMovable = movableOrderNumber;
                curCollider.GetComponent<MovableLogic>().MovableHitAndActivated();
                GameObject.FindWithTag("GameController").GetComponent<GameController>().CheckIfGoalReached(currentMovable);
                GameObject.FindWithTag("MusicManager").GetComponent<PlaybackEngine>().TriggerFromGame(curCollider.gameObject);
            } else if (movableOrderNumber > currentMovable)
            {
                ResetCurrentMovable();
                GameObject.FindWithTag("GameController").GetComponent<GameController>().ResetMovableColors();
            }
        }
    }

}

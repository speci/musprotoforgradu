﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaybackEngine : MonoBehaviour {

    private bool running = false;
    private double nextEventTime;
    private double nextTinyEventTime;
    private double latestTinyEventTime;

    public Song currentSong;

    private double oneBar;
    private double fourthBeat;
    private double eightBeat;
    private int tahtilaji = 4;
    private int barDividedInto = 8;

    [SerializeField] private int barIndex;
    [SerializeField] private int fourthBeatIndex;
    [SerializeField] private int eightBeatIndex;

    public bool tempoClick;
    public List<Instrument> tempoClickInstruments;

    private Track currentActiveTrack;
    private ManagerUI myUI;

    private List<Instrument> asynchronousQueue;
    private bool firstTime = true;

    void Start() {
        fourthBeat = 60.0F / currentSong.GiveTempo();
        eightBeat = fourthBeat / 2;
        oneBar = fourthBeat * tahtilaji;
        currentActiveTrack = currentSong.tracks[0];
        myUI = GetComponent<ManagerUI>();

        asynchronousQueue = new List<Instrument>();
        running = false;
    }

    internal void SetEngineActive(bool value)
    {
        if (value) {
            nextEventTime = AudioSettings.dspTime + 2.0;
            nextTinyEventTime = nextEventTime;
        }
        running = value;
    }

    private void PlayClick(double time)
    {
        for (int i = 0; i < tahtilaji; i++)
        {
            double timme = time + fourthBeat * i;
            if (i == 0) {
                tempoClickInstruments[0].PlayNote(Note.defNote, null, timme);
            }
            else {
                tempoClickInstruments[1].PlayNote(Note.defNote, null, timme);
            }
        }
    }

    /// <summary>
    /// Trigger a random instrument.
    /// </summary>
    internal void TriggerFromGame(GameObject output)
    {
        //int instrumentIndex = currentSong.GiveRandomInstrumentByType(Instrument.InstrumentType.Melodic);
        // Trigger an instrument that has been set to the hit movable.
        string noteIndex = "" + output.GetComponent<MovableLogic>().instrumentIndex + (barIndex % 2) + (eightBeatIndex % 8);
        GetComponent<ManagerUI>().TriggerSequencer(noteIndex, output);
    }

    private void PutNoteToClosest(double currentBarStart, double triggered, Instrument instrument, Note nootti)
    {
        double smallestSubtract = double.MaxValue;
        int closestNoteIndex = 0;

        for (int i = 0; i < (barDividedInto + 1); i++)
        {
            double closeCandidate = currentBarStart + (eightBeat * i);
            double subs = Math.Abs(triggered - closeCandidate);
            if (subs < smallestSubtract)
            {
                smallestSubtract = subs;
                // I don't know why the tempo click is eightBeat late (or ahead)
                closestNoteIndex = i - 1;
            }
        }

        // Put note to the first hit of the next bar.
        currentActiveTrack = currentSong.GiveTrackByInstrument(instrument);
        if (closestNoteIndex >= barDividedInto) currentActiveTrack.PutNote(barIndex - 1, 0, nootti);
        else if ( closestNoteIndex < 0 ) currentActiveTrack.PutNote(barIndex + 1, 0, nootti);
        else currentActiveTrack.PutNote(barIndex - 2, closestNoteIndex, nootti); 
    }

	void Update () {
        if (!running) return;

        double time = AudioSettings.dspTime;

        // Give note play times every eight beat (or whatever is the smallest time)
        if (time > nextTinyEventTime) {
            myUI.TimeTickMessage(eightBeatIndex, barIndex, -1);

            for (int i = 0; i < currentSong.tracks.Count; i++)
            {
                Track curTrack = currentSong.tracks[i];
                Bar bar = curTrack.giveBar(barIndex);
                Note note = bar.GetNote(eightBeatIndex);

                if (note != null) // If a playable note was found
                {
                    double nextTime = nextTinyEventTime + eightBeat;
                    List<Note> curChord = currentSong.chordTrack.GetChordNotes(barIndex, eightBeatIndex);
                    curTrack.instrument.PlayNote(note, curChord, nextTime);
                }
            }
            
            if (eightBeatIndex % 2 == 0 && eightBeatIndex != 0)
            {
                fourthBeatIndex++;
            }

            eightBeatIndex++;
            if (eightBeatIndex >= 8)    // One bar has been played
            {
                eightBeatIndex = 0;
                barIndex++;

                if (barIndex > 1)   // Currently only two bars so reset index after reaching 2
                {
                    barIndex = 0;
                }
            }
            nextTinyEventTime += eightBeat;
        }
    }

}

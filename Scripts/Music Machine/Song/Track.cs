﻿using System.Collections.Generic;
using UnityEngine;

public class Track {

    public List<Bar> bars;
    public bool mute;
    public Instrument instrument;

    public Track()
    {
        bars = new List<Bar>();

        for (int i = 0; i < 2/*4*/; i++)
        {
            bars.Add(new Bar());
        }
    }

    internal Bar giveBar(int barIndex)
    {
        return bars[barIndex % bars.Count];
    }

    public Note Pulse(int noteTick, int bar)
    {
        return bars[bar % bars.Count].GetNote(noteTick);
    }

    internal void PutNote(int barIndex, int closestNoteIndex, Note note)
    {
        if (barIndex < 0) barIndex = 0;
        //Debug.Log("bar " + (barIndex % bars.Count) + " beat " + closestNoteIndex);
        if (instrument.type == Instrument.InstrumentType.Rhythm) bars[barIndex % bars.Count].notes[closestNoteIndex] = new Note(1f);
        else bars[barIndex % bars.Count].notes[closestNoteIndex] = note;
    }

    internal void ClearTrack()
    {
        for (int i = 0; i < bars.Count; i++)
        {
            bars[i].ClearBar();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Song : MonoBehaviour {

    public string songName;
    public List<Track> tracks;
    public List<Instrument> instrumentPrefabs;

    internal List<Instrument> songInstruments;

    public int tempoMin;
    public int tempoMax;

    public bool empty;

    public ChordTrack chordTrack;

    private void Awake()
    {
        InitializeInstruments();
        if (empty) EmptySong();
        else SampleSong();
    }

    private void Start()
    {
        InitializeChordTrack();
    }

    private void InitializeInstruments()
    {
        songInstruments = new List<Instrument>();
        foreach (var instr in instrumentPrefabs)
        {
            songInstruments.Add(Instantiate(instr));
        }
    }

    // Add or remove a note.
    // Removing is done by setting the volume to 0.
    internal string TriggerInstrument(int track, int bar, int note, GameObject output)
    {
        Track curTrack = tracks[track];
        string removed = "";

        // Remove a note if already exists. A feature
        if (curTrack.bars[bar].notes[note].volume > 0f)
        {
            //curTrack.bars[bar].notes[note].volume = 0f;
            RemoveNote(curTrack, bar, note);
            curTrack.instrument.AddNotesActivatedAmount(-1);
        }
        else
        {
            // Remove a random note if the limited amount has been exceeded.
            if (curTrack.instrument.limited && curTrack.instrument.limitAmount <= curTrack.instrument.GetNotesActivatedAmount())
            {
                removed = "" + track + RemoveRandomNoteFrom(curTrack);
                curTrack.instrument.AddNotesActivatedAmount(-1);
            }

            // Add a note.
            if (curTrack.instrument.playBass) AddNote(curTrack, bar, note, GetComponent<ScaleInfo>().GiveBassNote(chordTrack.GetChord(bar, note))/*, output*/);
            else AddNote(curTrack, bar, note, GetComponent<ScaleInfo>().GiveRandomAppropriateNote(chordTrack.GetChord(bar, note))/*, output*/);
            curTrack.instrument.AddNotesActivatedAmount(1);
        }
        return removed;
    }

    internal List<int> GiveInstrumentsByType(Instrument.InstrumentType type)
    {
        List<int> instru = new List<int>();

        for (int i = 0; i < tracks.Count; i++)
        {
            if (GiveInstrument(i).type == type) instru.Add(i);
        }
        return instru;
    }

    internal int GiveRandomInstrumentByType(Instrument.InstrumentType type)
    {
        List<int> instrumentsByType = GiveInstrumentsByType(type);
        return instrumentsByType[UnityEngine.Random.Range(0, instrumentsByType.Count)];
    }

    private void AddNote(Track track, int bar, int note, Note songNote)
    {
        track.bars[bar].notes[note] = songNote;
        //track.bars[bar].notes[note].AddOutput(output);
    }

    /// <summary>
    /// Removes a random note from given track.
    /// </summary>
    /// <param name="track"></param>
    /// <returns>Bar and note indeces</returns>
    private string RemoveRandomNoteFrom(Track track)
    {
        List<string> notes = new List<string>();
        string removed;

        for (int i = 0; i < track.bars.Count; i++)
        {
            for (int j = 0; j < track.bars[i].notes.Length; j++)
            {
                if (track.bars[i].notes[j].volume > 0) notes.Add("" + i + j);
            }
        }
        //print(notes.Count);
        removed = notes[UnityEngine.Random.Range(0, notes.Count)];
        //print("poistetaan " + removed);
        RemoveNote(track, int.Parse(removed.Substring(0, 1)), int.Parse(removed.Substring(1, 1)) );
        return removed;
    }

    /// <summary>
    /// Removes a single note.
    /// </summary>
    /// <param name="track"></param>
    /// <param name="bar"></param>
    /// <param name="note"></param>
    private void RemoveNote(Track track, int bar, int note)
    {
        track.bars[bar].notes[note].volume = 0f;
    }

    private void InitializeChordTrack()
    {
        if (chordTrack != null) chordTrack.UpdateChords();
    }

    internal int GiveTempo()
    {
        return UnityEngine.Random.Range(tempoMin, tempoMax);
    }

    internal void ClearNotes()
    {
        for (int i = 0; i < tracks.Count; i++)
        {
            for (int j = 0; j < tracks[i].bars.Count; j++)
            {
                for (int k = 0; k < tracks[i].bars[j].notes.Length; k++)
                {
                    tracks[i].bars[j].notes[k].volume = 0f;
                }
            }
        }
    }

    private void EmptySong()
    {
        tracks = new List<Track>();

        for (int i = 0; i < songInstruments.Count; i++)
        {
            Track myTrack = new Track();
            myTrack.instrument = songInstruments[i];
            tracks.Add(myTrack);
        }

    }

    private void SampleSong()
    {
        print("Creating a sample song");
        //Song sampleSong = new Song();
        //sampleSong.tempo = tempo;
        //sampleSong.instruments = instruments;
        tracks = new List<Track>();

        for (int i = 0; i < songInstruments.Count; i++)
        {
            Track myTrack = new Track();
            myTrack.instrument = songInstruments[i % songInstruments.Count];
            tracks.Add(myTrack);
        }

        // Add first track, bass drum
        //Track myTrack = new Track();
        //myTrack.instrument = instruments[0];
        //tracks.Add(myTrack);

        //Note myNote = new Note();
        //myNote.volume = 0.4f;
        ////myNote.octave = 1;
        //myNote.octave = 0;
        //myNote.note = Notes.A;
        tracks[0].bars[0].notes[0] = new Note(Notes.A, 1f, 1);
        tracks[0].bars[0].notes[7] = new Note(Notes.A, 1f, 1);
        tracks[0].bars[1].notes[1] = new Note(Notes.A, 1f, 1);

        // Add second track, snare
        Track myTrack2 = new Track();
        myTrack2.instrument = songInstruments[1];
        tracks.Add(myTrack2);

        //Note myNote23 = new Note();
        //myNote23.volume = 1f;
        ////myNote23.octave = 1;
        //myNote23.octave = 0;
        //myNote23.note = Notes.A;
        tracks[3].bars[0].notes[2] = new Note(Notes.A, 1f, 1);
        tracks[2].bars[1].notes[2] = new Note(Notes.A, 1f, 1);

        tracks[1].bars[0].notes[4] = new Note(Notes.G);
        tracks[1].bars[0].notes[5] = new Note(Notes.E);
        tracks[1].bars[0].notes[6] = new Note(Notes.D);

        tracks[3].bars[1].notes[4] = new Note(Notes.G);
        tracks[3].bars[1].notes[5] = new Note(Notes.E);
        tracks[3].bars[1].notes[6] = new Note(Notes.D);
        tracks[3].bars[1].notes[7] = new Note(Notes.C);

        //return sampleSong;
    }

    internal Instrument GiveInstrument(int index)
    {
        return songInstruments[index % songInstruments.Count];
    }

    internal void ClearTrackByNumber(int trackNumber)
    {
        if (trackNumber <= 0 && trackNumber < tracks.Count) tracks[trackNumber].ClearTrack();
    }

    internal Track GiveTrackByNumber(int trackNumber)
    {
        if (trackNumber <= 0 && trackNumber < tracks.Count) return tracks[trackNumber];
        return null;
    }

    internal Track GiveTrackByInstrument(Instrument instrument)
    {
        for (int i = 0; i < tracks.Count; i++)
        {
            if (tracks[i].instrument == instrument) return tracks[i];
        }
        return tracks[0];
    }
}

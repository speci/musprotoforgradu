﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note {

    public float volume;
    public Notes note;
    public int octave;
    internal List<GameObject> outputs;

    public static Note defNote = new Note(1f);

    public Note()
    {
        volume = 0f;
        outputs = new List<GameObject>();
    }

    //internal void MapOutputs(double delay)
    //{
    //    if (outputs.Count > 0)
    //    {
    //        float time = (float)(delay - AudioSettings.dspTime);
    //        for (int i = 0; i < outputs.Count; i++)
    //        {
    //            outputs[i].GetComponent<MovableLogic>().MoveOutput();
    //            //Debug.Log("map");
    //        }
    //    }
    //}

    //internal void AddOutput(GameObject output)
    //{
    //    outputs.Add(output);
    //}

    public Note(float volume)
    {
        this.volume = volume;
        note = Notes.A;
        outputs = new List<GameObject>();
    }

    public Note(int note)
    {
        this.note = (Notes)note;
        volume = 1f;
        outputs = new List<GameObject>();
    }

    public Note(Notes note, float volume, int octave)
    {
        this.note = note;
        this.volume = volume;
        this.octave = octave;
        outputs = new List<GameObject>();
    }

    public Note(Note note)
    {
        this.note = note.note;
        volume = note.volume;
        octave = note.octave;
        outputs = new List<GameObject>();
    }

    public Note(Notes note)
    {
        this.note = note;
        volume = 1f;
        octave = 0;
        outputs = new List<GameObject>();
    }

    internal void ClearNote()
    {
        volume = 0f;
    }

    //public static Note DefNote()
    //{
    //    Note de = new Note();
    //    de.volume = 1;
    //    return de;
    //}

}

//public enum Notes{ C = 0, CSH, D, DSH, E, F, FSH, G, GSH, A, ASH, H };
public enum Notes { A = 0, ASH, H, C, CSH, D, DSH, E, F, FSH, G, GSH, };

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar {

    public Note[] notes;
    // TODO Get this from PlayBackEngine
    public int divided = 8;

    public Bar()
    {
        notes = new Note[divided];

        for (int i = 0; i < notes.Length; i++)
        {
            notes[i] = new Note();
            notes[i].volume = 0f;
        }
    }

    public Note GetNote(int noteTick)
    {
        if (notes[noteTick] != null)
        {
            if (notes[noteTick].volume > 0f) return notes[noteTick];
        }
        return null;
    }

    internal void ClearBar()
    {
        for (int i = 0; i < notes.Length; i++)
        {
            notes[i].ClearNote();
        }
    }

}

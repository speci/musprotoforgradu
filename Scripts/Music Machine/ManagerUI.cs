﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour {

    public Text barText;
    [SerializeField] private GameObject sendMessagesTo;

    private Song currentSong;

    private void Start()
    {
        currentSong = GetComponent<PlaybackEngine>().currentSong;
    }

    internal void TimeTickMessage(int timeTickSignature, int barIndex, int nudge)
    {
        int[] indeces = new int[] { timeTickSignature, barIndex, nudge };
        if (sendMessagesTo != null)
        {
            sendMessagesTo.SendMessage("Tick", indeces, 0);
        }
    }

    internal void SetBarText(string text)
    {
        if (barText != null) barText.text = text;
    }

    internal void TriggerSequencer(int buttonNumber)
    {
        TriggerSequencer("" + buttonNumber, null);
    }

    internal void TriggerSequencer(string buttonNumber)
    {
        TriggerSequencer(buttonNumber, null);
    }

    internal void TriggerSequencer(string buttonNumber, GameObject output)
    {
        int i = int.Parse(buttonNumber.Substring(0, 1));    // Track
        int k = int.Parse(buttonNumber.Substring(1, 1));    // Bar
        int j = int.Parse(buttonNumber.Substring(2, 1));    // Note of bar

        int divi = currentSong.tracks[0].bars[0].divided;

        string removed = AddNoteToSong(i, k, j, output);

        if (!removed.Equals("")) sendMessagesTo.GetComponent<TracksTenoriUI>().TriggerSequencer(removed);
        sendMessagesTo.GetComponent<TracksTenoriUI>().TriggerSequencer(buttonNumber, output);
    }

    private string AddNoteToSong(int i, int k, int j, GameObject output)
    {
        return currentSong.TriggerInstrument(i, k, j, output);
    }

}

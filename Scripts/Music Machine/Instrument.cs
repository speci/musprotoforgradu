﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instrument : MonoBehaviour
{
    public string instrumentName;

    public enum InstrumentType { Rhythm, Melodic };
    public InstrumentType type;
    public bool playChords;
    public bool playBass;

    public AudioClip sound;

    public GameObject audioPlay;

    public bool limited;
    public int limitAmount;
    private int notesActivatedAmount;

    internal int GetNotesActivatedAmount()
    {
        return notesActivatedAmount;
    }

    void Start()
    {
        notesActivatedAmount = 0;
    }

    internal void AddNotesActivatedAmount(int amount)
    {
        notesActivatedAmount += amount;
    }

    public void PlayNote(Note note, List<Note> currentChord, double time)
    {
        if (type == InstrumentType.Melodic)
        {
            if (playChords)
            {
                foreach (var chordNote in currentChord)
                {
                    PlayNote(chordNote, time);
                }
            }
            else
            {
                PlayNote(note, time);
            }
        }
        else
        {
            PlayNote(new Note(Notes.D), time);
        }
    }

    private void PlayNote(Note note, double time)
    {
        GameObject auGameObject = ObjectPoolManager.GetNext(audioPlay);
        AudioPlayObject audioPlayObject = auGameObject.GetComponent<AudioPlayObject>();
        audioPlayObject.Play(sound, note, time);
        //note.MapOutputs(time);
    }

}

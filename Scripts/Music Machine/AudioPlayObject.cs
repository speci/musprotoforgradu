﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayObject : MonoBehaviour {

    private AudioSource mySource;
    //private int transpose = -4;

    private void OnEnable()
    {
        mySource = GetComponent<AudioSource>();
        mySource.pitch = 1;
    }

    internal void Play(AudioClip sound, Note note)
    {
        mySource.clip = sound;
        mySource.pitch = Transpose( (int)note.note + (note.octave * 12));
        mySource.Play();
        StartCoroutine(WaitAndDisable(mySource.clip.length));
    }

    private IEnumerator WaitAndDisable(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    internal void Play(AudioClip sound, Note note, double time)
    {
        //print("oktaavi " + note.octave);
        mySource.clip = sound;
        mySource.pitch = Transpose( (int)note.note + (note.octave * 12));
        mySource.PlayScheduled(time);
        StartCoroutine(WaitAndDisable(5f));
    }

    private float Transpose(int note)
    {
        //print(" transpose sai nuotin " + note);
        float tran = Mathf.Pow(2, (note /*+ transpose*/) / 12.0f);
        //print("transpose return: " + tran);
        return tran;
    }
}

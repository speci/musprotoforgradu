﻿using UnityEngine;
using System.Collections;
using System;

public class OldMelody : MonoBehaviour {

    public OldSampler.Notes[] startNotes = { OldSampler.Notes.C1,
                                          OldSampler.Notes.D1,
                                          OldSampler.Notes.E1,
                                          OldSampler.Notes.F1,
                                          OldSampler.Notes.G1,
                                          OldSampler.Notes.A1,
                                          OldSampler.Notes.H1};
    public int startNoteIndex = 0;
    public OldSampler.Notes currentStartNote;

    public int[] melody;
    public int melodyIndex = 0;
    
    // TODO The note shold be the same when starting from C again but it isn't.
    public int GetNoteToPlay()
    {
        OldSampler.Notes note = (OldSampler.Notes) ( ((int)currentStartNote + melody[melodyIndex++ % melody.Length]) % Enum.GetNames(typeof(OldSampler.Notes)).Length);
        //Debug.Log("Note " + (int)note);
        //return (int)startNotes[startNoteIndex % startNotes.Length] + melody[melodyIndex++ % melody.Length];
        return (int)note;
    }
    
    public void RaiseStartNote()
    {
        //Debug.Log("------------------------");
        startNoteIndex++;
        currentStartNote = startNotes[startNoteIndex % startNotes.Length];
        melodyIndex = 0;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class OldSampler : MonoBehaviour {

    //                {     0    1  2    3  4  5    6  7    8  9   10 11}  
	public enum Notes {C1 = 0,CSH1,D1,DSH1,E1,F1,FSH1,G1,GSH1,A1,ASH1,H1};
	public Notes scale;

	private List<List<int>> chordsInt;
	private List<List<Notes>> chords;

	private List<Notes> notesList;
	public GameObject clipPlayer;
	public List<AudioClip> samples;
    // Maybe this is right 0.05
	//private float transpose = 0.05f;
    private float transpose = 0.055f;
    public float waitBetweenNotes;
    private OldMelody melody;
    private AudioSource audioSource;
    public int noteIndex = 0;

	void Start() {
        melody = GetComponent<OldMelody>();
        audioSource = GetComponent<AudioSource>();
		notesList = new List<Notes> {Notes.C1, Notes.CSH1, 
									 Notes.D1, Notes.DSH1, Notes.E1, 
									 Notes.F1, Notes.FSH1, Notes.G1, 
									 Notes.GSH1, Notes.A1, Notes.ASH1, 
									 Notes.H1};
//		// C,E,G
//		int[] firstChord = {1,5,8};
//		// D,F,A
//		int[] secondChord = {3,6,10};
//		// C,F,A
//		int[] fourthChord = {1,6,10};
//		// D,G,H
//		int[] fifthChord = {3,8,12};

		// In C: C,E,G
		int[] firstChord = {0,4,7};
		// D,F,A
		int[] secondChord = {2,5,9};
		// C,F,A
		int[] fourthChord = {0,5,9};
		// D,G,H
		int[] fifthChord = {2,7,11};

		chordsInt = new List<List<int>>();
		chordsInt.Add(new List<int>(firstChord));
		chordsInt.Add(new List<int>(secondChord));
		chordsInt.Add(new List<int>(fourthChord));
		chordsInt.Add(new List<int>(fifthChord));

		chords = getChords();
	}

    //void OnMouseDown()
    //{
    //    StartCoroutine(PlaySound());
    //}

    private IEnumerator PlayNote()
    {
        //audioSource.PlayOneShot(melody.GetNoteToPlay());
        yield return new WaitForSeconds(waitBetweenNotes * Time.deltaTime);
    }

//	void OnMouseDown() {
//		float pitch = 1f;
//		int noteIndex = 0;

////		List<Notes> playNotes = notesList;
////		amountOfNotes = notesList.Count;
////		List<Notes> playNotes = randomNotes(amountOfNotes);

//		List<Notes> playNotes = randomChord();
//		chordIndex ++;
//		StartCoroutine(playSound(pitch, playNotes, noteIndex, playNotes.Count));
//	}

	private List<List<Notes>> getChords(){
		List<List<Notes>> chordsList = new List<List<Notes>>();
		
		foreach(List<int> chordInt in chordsInt) {
			List<Notes> chord = new List<Notes>();
			
			foreach(int noteInt in chordInt) {
				int moi = (noteInt + (int)scale ) % notesList.Count;
//				Debug.Log ((Notes) moi + " " + moi);
				chord.Add((Notes)moi);
			}
			chord = organizeChord(chord);
			chordsList.Add(chord);
		}
		return chordsList;
	}

	private List<Notes> organizeChord(List<Notes> chord) {
		for(int i = 0; i < chord.Count -1; i++) {
			for (int j = i + 1; j < chord.Count; j ++) {
				if ((int)chord[i] > (int)chord[j]) {
					Notes move = chord[j];
					chord[j] = chord[i];
					chord[i] = move;
				}
			}
		}
		return chord;
	}

	private List<Notes> randomNotes(int amountOfNotes) {
		List<Notes> playNotes = new List<Notes>();
		
		for(int i = 0; i < amountOfNotes; i++) {
			playNotes.Add(notesList[UnityEngine.Random.Range(0, notesList.Count)]);
		}
		return playNotes;
	}

    //private IEnumerator PlaySound(float pitch, List<Notes> playNotes, int noteIndex, int amountOfNotes) {
    //	GameObject clipP = Instantiate(clipPlayer, Vector3.zero, Quaternion.identity) as GameObject;
    //	AudioSource clipAudio = clipP.GetComponent<AudioSource>();
    //	Debug.Log(playNotes[noteIndex]);
    //	getNoteClip(playNotes[noteIndex], clipAudio);
    //	clipAudio.Play();

    //	yield return new WaitForSeconds(waitBetweenNotes);
    //	if (noteIndex < playNotes.Count - 1) {
    //		StartCoroutine(playSound(pitch, playNotes, noteIndex + 1, playNotes.Count));
    //	}
    //}

    public void PlaySound()
    {
        StartCoroutine(PlaySoundInternal());
    }

    private IEnumerator PlaySoundInternal()
    {
        GameObject clipPlayer = CFX_SpawnSystem.GetNextObject(this.clipPlayer, false);
        AudioSource clipAudio = clipPlayer.GetComponent<AudioSource>();
        GetNoteClip((Notes)(melody.GetNoteToPlay() % (Enum.GetNames(typeof(Notes)).Length)), clipAudio);
        clipPlayer.SetActive(true);
        clipAudio.Play();
        //clipAudio.GetComponent<ClipPlayerScript>().WaitAndDisable();

        yield return new WaitForSeconds(waitBetweenNotes);
        if ( (noteIndex == 0 && melody.melody.Length != 1) || !((noteIndex % melody.melody.Length) == 0))
        {
            noteIndex++;
            StartCoroutine(PlaySoundInternal());
        }
        else
        {
            noteIndex = 0;
            melody.RaiseStartNote();
        }
    }

    private void GetNoteClip(Notes note, AudioSource audio) {
        //Debug.Log("Haku " + (int)note);
        switch (note) {
			case Notes.C1:
				audio.clip = samples[0];
				break;
			case Notes.CSH1:
				audio.clip = samples[0];
				audio.pitch = 1 + transpose;
				break;
	
			case Notes.D1:
				audio.clip = samples[1];
				audio.pitch = 1 - transpose;
				break;
			case Notes.DSH1:
				audio.clip = samples[1];
				break;
			case Notes.E1:
				audio.clip = samples[1];
				audio.pitch = 1 + transpose;
				break;

			case Notes.F1:
				audio.clip = samples[2];
				audio.pitch = 1 - transpose;
				break;
			case Notes.FSH1:
				audio.clip = samples[2];
				break;
			case Notes.G1:
				audio.clip = samples[2];
				audio.pitch = 1 + transpose;
				break;

			case Notes.GSH1:
				audio.clip = samples[3];
				audio.pitch = 1 - transpose;
				break;
			case Notes.A1:
				audio.clip = samples[3];
				break;
			case Notes.ASH1:
				audio.clip = samples[3];
				audio.pitch = 1 + transpose;
				break;

            // Returning to C.
    			case Notes.H1:
				audio.clip = samples[0];
				audio.pitch = 1 - transpose;
				break;
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleInfo : MonoBehaviour {

    public NoteScale currentScale;
    public Notes baseNote;

    public List<NoteScale> allScales;

    [SerializeField]
    private int currentScaleIndex;

    public Note GiveRandomAppropriateNote()
    {
        int scaleCount = currentScale.notesInTheScale.Count;
        int theNote = (int)baseNote + currentScale.notesInTheScale[UnityEngine.Random.Range(0, scaleCount)];

        Note nootti = new Note();
        nootti.volume = 1f;
        nootti.note = (Notes)theNote;
        return nootti;
    }

    public Note GiveRandomAppropriateNote(Chord chord)
    {
        currentScale = GetScale(chord.type);
        int baseNote = (int)chord.baseNote;

        int scaleCount = currentScale.notesInTheScale.Count;
        int theNote = (int)baseNote + currentScale.notesInTheScale[UnityEngine.Random.Range(0, scaleCount)];

        Note nootti = new Note();
        nootti.volume = 1f;
        nootti.note = (Notes)theNote;
        //print("base note: " + chord.baseNote + " note " + nootti.note);
        return nootti;
    }

    private NoteScale GetScale(Chords type)
    {
        for (int i = 0; i < allScales.Count; i++)
        {
            if (allScales[i].name.StartsWith(type.ToString(), true, null)) return allScales[i];
        }
        return allScales[2];    // Return major scale
    }

    public Note GiveInScale(int noteIndex)
    {
        int scaleCount = currentScale.notesInTheScale.Count;
        int theNote = (int)baseNote + currentScale.notesInTheScale[noteIndex % scaleCount];

        currentScaleIndex++;

        Note nootti = new Note();
        nootti.volume = 1f;
        nootti.note = (Notes)theNote;
        return nootti;
    }

    internal Note GiveBassNote(Chord chord)
    {
        currentScale = GetScale(chord.type);
        int baseNote = (int)chord.baseNote;

        int scaleCount = currentScale.notesInTheScale.Count;
        int theNote = (int)baseNote;

        Note nootti = new Note();
        nootti.volume = 1f;
        nootti.note = (Notes)theNote;
        //print("base note: " + chord.baseNote + " note " + nootti.note);
        return nootti;
    }

    public Note GiveNextInScale()
    {
        int scaleCount = currentScale.notesInTheScale.Count;
        int theNote = (int)baseNote + currentScale.notesInTheScale[currentScaleIndex % scaleCount];
        currentScaleIndex++;

        Note nootti = new Note();
        nootti.volume = 1f;
        nootti.note = (Notes)theNote;
        nootti.octave = ((theNote - 1) / 12); // If a note's the 13th note or above, add octaves accordingly
        //print("lot: " + nootti.note + " oktaavi: " + nootti.octave);
        return nootti;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChordBar : MonoBehaviour {

    public Chord[] chordTimes;
    // TODO Get this from PlayBackEngine
    public int divided = 8;

    public ChordBar()
    {
        chordTimes = new Chord[divided];
    }

}

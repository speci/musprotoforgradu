﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChordTrack : MonoBehaviour {

    internal List<ChordBar> chordBars;
    public List<Chord> chords;

    private Dictionary<Chord, List<Note>> savedChords;
    public List<int> changeChordsAt;

    public ChordTrack()
    {
        chordBars = new List<ChordBar>();

        for (int i = 0; i < 2/*4*/; i++)
        {
            chordBars.Add(new ChordBar());
        }
    }

    internal void UpdateChords()
    {
        int chordIndex = 0;

        for (int i = 0; i < chordBars.Count; i++)
        {
            for (int j = 0; j < chordBars[i].divided; j++)
            {
                PutChord(i, j, chords[chordIndex % chords.Count]);
                // Plus 1 chord index if time to change chord
                int temp = (i * 8 + j) + 1;
                if ( chordIndex < changeChordsAt.Count && temp == changeChordsAt[chordIndex]) chordIndex++;
                //print(temp + " " + chordIndex);
            }
        }
        SaveChords();
    }

    private void SaveChords()
    {
        savedChords = new Dictionary<Chord, List<Note>>();

        for (int i = 0; i < chordBars.Count; i++)
        {
            for (int j = 0; j < chordBars[i].chordTimes.Length; j++)
            {
                Chord currentChord = GetChord(i,j);
                if (!savedChords.ContainsKey(currentChord))
                {
                    List<Note> chordNotes = currentChord.GetChordAsNotes();
                    savedChords.Add(currentChord, chordNotes);
                }
            }
        }
    }

    internal void PutChord(int barIndex, int chordIndex, Chord chord)
    {
        if (barIndex < 0) barIndex = 0;
        chordBars[barIndex % chordBars.Count].chordTimes[chordIndex] = chord;
    }

    internal Chord GetChord(int barIndex, int chordIndex)
    {
        ChordBar myBar = chordBars[barIndex % chordBars.Count];
        return myBar.chordTimes[chordIndex % myBar.chordTimes.Length];
    }

    internal List<Note> GetChordNotes(int barIndex, int chordIndex)
    {
        Chord chord = GetChord(barIndex, chordIndex);
        if (savedChords.ContainsKey(chord)) return savedChords[chord];
        return null;
    }
}

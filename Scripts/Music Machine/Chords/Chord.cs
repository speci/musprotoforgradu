﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chord : MonoBehaviour {

    public Notes baseNote;
    public Notes bass;
    public Chords type;

    public bool seventh;
    public Seventh seventhType;

    public bool ninth;

    public Chord(Notes baseNote, Chords type)
    {
        this.baseNote = baseNote;
        bass = baseNote;
        this.type = type;
    }

    internal List<Note> GetChordAsNotes()
    {
        List<Note> chord = new List<Note>();

        chord.Add(new Note((int)bass));       // Bass note
        chord.Add(new Note((int)baseNote + 3 + (int)type));   // Minor, major, sus4
        chord.Add(new Note((int)baseNote + 7));               // Fifth

        if (seventh) chord.Add(new Note((int)baseNote + 9 + (int)seventhType));
        if (ninth) chord.Add(new Note((int)baseNote + 14));

        return chord;
    }

    //internal List<int> GetChord()
    //{
    //    List<int> chord = new List<int>();

    //    chord.Add((int)bass);       // Bass note
    //    chord.Add(3 + (int)type);   // Minor, major, sus4
    //    chord.Add(7);               // Fifth

    //    if (seventh) chord.Add(9 + (int)seventhType);
    //    if (ninth) chord.Add(14);

    //    return chord;
    //}

}

public enum Chords { minor = 0, major, sus4 }
public enum Seventh { dim = 0, seventh, majSeventh }